#!/usr/bin/env python

import sys

sys.path.append("../../../hw/hw7/")

import mapper_reducer


def reducefunc(emit, key, vals):
    vals_as_ints = map(int, vals)
    emit(key, sum(vals_as_ints))

mr = mapper_reducer.MapperReducer()
mr.reduce(reducefunc, sys.stdin)

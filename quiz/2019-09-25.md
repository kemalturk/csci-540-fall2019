# CRUD ops with Neo4J

Let's start off with some practice with the material we covered yesterday and
solve the "Do 1" question from Neo 4J - Day 1

Create a simple graph describing some of your closest friends, your
relationships with them, and even some relationships between your friends. Start
with three nodes, including one for yourself, and create five relationships.
